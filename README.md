# Time Averaged Projection - supporting code

This repository holds the time-averaged projection supporting code.

These notebooks are intended to be run on google colab, alternatively they can be run on a local PC with the required libraries installed. To use these files in google colab, download the files, visit https://colab.research.google.com/, and upload the files to run them.
